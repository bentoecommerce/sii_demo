import { AppComponent } from './app';

describe('AppComponent', () => {
    const TEST_TITLE = 'Ask Wikipedia';
    var app = new AppComponent();

    it('has a title', () => {
        expect(app.title).toEqual(TEST_TITLE);
    });

});

