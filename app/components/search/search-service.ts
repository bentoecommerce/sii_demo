import { Injectable } from '@angular/core';
import { Jsonp, URLSearchParams, Response } from '@angular/http';
import { Observable }     from 'rxjs/Rx';
import { Suggestion } from '../../model/suggestion';

@Injectable()
export class SearchService {
    private wikiUrl = 'http://en.wikipedia.org/w/api.php';//' https://en.wikipedia.org/w/api.php?action=opensearch&search=Te&limit=4'; 

    constructor (private jsonp: Jsonp) {}
    
    getSearchResultsForPrefix(searchValue: string, limit: number) : Observable<Suggestion[]> { 
        let params = new URLSearchParams();
        params.set('search', searchValue); 
        params.set('action', 'opensearch');
        params.set('format', 'json');
        params.set('limit', limit.toString());
        params.set('callback', 'JSONP_CALLBACK');
    
        return this.jsonp
                .get(this.wikiUrl, { search: params })
                .map(response => {
                    let data = response.json();
                    let descriptionIterator = data[2][Symbol.iterator]();
                    let urlIterator = data[3][Symbol.iterator]();
                    return <Suggestion[]> data[1].map(
                        suggestion => new Suggestion(suggestion, descriptionIterator.next().value, urlIterator.next().value)) 
                })
                .catch(this.handleError);
    }


    private handleError (error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

}