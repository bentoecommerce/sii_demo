import { SearchComponent } from './search';
import { Observable }     from 'rxjs/Rx';
import { Suggestion } from '../../model/suggestion';

describe ('Search component', () => {

    const TEST_TERM = 'test term';
    let searchService, searchComponent;
    const SUGGESTIONS = [new Suggestion(TEST_TERM, 'TEST_DESCRIPTION', 'TEST_URL')];
    
    beforeEach(() => {
        searchService = jasmine.createSpyObj('SearchService', ['getSearchResultsForPrefix']); 
        searchComponent = new SearchComponent(searchService);
    });

    describe('Search Component init', () => {
        it('is initialized with an empty suggestions list', () => {
            expect(searchComponent.suggestions.length).toBe(0);
            expect(searchComponent.term.value).toBe(null);
        });
    });

    describe('Search executed', () => {
        beforeEach( (done) => {
            // ! to musi sie dac zrobic lepiej ;)
            searchService.getSearchResultsForPrefix.and.callFake(() => { setTimeout(() => {done()}, 20); return Observable.of(SUGGESTIONS);});
            searchComponent.term.updateValue(TEST_TERM, { emitEvent : true });
        });

        it('has filled suggestions list', () => {
            expect(searchComponent.suggestions.length).toBe(1);
            expect(searchComponent.suggestions[0].title).toBe(TEST_TERM);
        });
    });
});