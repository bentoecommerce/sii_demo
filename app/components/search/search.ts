import { Component } from '@angular/core';
import { SearchService } from './search-service';
import { Suggestion } from './../../model/suggestion';
import { SuggestionListComponent } from './../suggestion-list/suggestion-list';
import { Control } from '@angular/common';

@Component({ selector: 'search',  templateUrl: './app/components/search/search.html', providers: [SearchService], directives: [SuggestionListComponent]})
export class SearchComponent {
    suggestions: Suggestion[] = [];
    term = new Control();
    
    constructor(private searchService: SearchService){
         this.term.valueChanges
             .debounceTime(300).distinctUntilChanged().flatMap(term => searchService.getSearchResultsForPrefix(term, 20))
             .subscribe(suggestions => this.suggestions = suggestions);
    }
}



