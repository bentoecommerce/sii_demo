import { Component, Input } from '@angular/core';
import { Suggestion } from './../../model/suggestion';
import { SuggestionComponent } from './../suggestion/suggestion';

@Component({ selector: 'suggestion-list', templateUrl: './app/components/suggestion-list/suggestion-list.html', directives: [SuggestionComponent]})
export class SuggestionListComponent{
    constructor(){}
    
    @Input()
    suggestions: Suggestion[];
}
