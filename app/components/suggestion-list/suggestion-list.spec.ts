import { SuggestionListComponent } from './suggestion-list';

describe('SuggestionListComponent', () => {
    const SUGGESTIONS = [];
   
    it('is empty when suggestions are not provided', () => {
        let suggestionList: SuggestionListComponent = new SuggestionListComponent();
        expect(suggestionList.suggestions).toEqual([]);
    });
   
});