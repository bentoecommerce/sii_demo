import { Component, Input } from '@angular/core';
import { Suggestion } from './../../model/suggestion';

@Component({ selector: 'suggestion', templateUrl: './app/components/suggestion/suggestion.html'})
export class SuggestionComponent {
    @Input() 
    suggestion: Suggestion;
}
