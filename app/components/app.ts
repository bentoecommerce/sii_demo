import { Component } from '@angular/core';
import { SearchComponent } from './search/search';

@Component({ selector: 'my-app', templateUrl: './app/components/app.html' ,  directives:[SearchComponent]})
export class AppComponent {
    public title = 'Ask Wikipedia';
}