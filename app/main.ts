import { bootstrap } from '@angular/platform-browser-dynamic';
import { AppComponent } from './components/app';
import { JSONP_PROVIDERS } from '@angular/http';
import { ROUTER_DIRECTIVES } from '@angular/router';

bootstrap(AppComponent, [JSONP_PROVIDERS]);
