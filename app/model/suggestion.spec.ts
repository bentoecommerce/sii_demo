import { Suggestion } from './suggestion';

describe('Suggestion', () => {
    const TEST_TITLE = 'Test title';
    const TEST_DESCR = 'Test description';
    const TEST_URL = 'http://test.url';
    
    var suggestion = undefined;
    
    describe('with no provided parameters',() => {

        beforeEach ( () => {
            suggestion = new Suggestion(undefined, undefined, undefined);
        });

        it('has no title', () => {     
            expect(suggestion.title).toBe(undefined);
        });

        it('has no description', () => {
            expect(suggestion.description).toBe(undefined);
        });

        it('has no url', () => {
            expect(suggestion.url).toBe(undefined);
        });
    });

    describe('with provided all the parameters', () => { 
        beforeEach ( () => { 
            suggestion = new Suggestion(TEST_TITLE, TEST_DESCR, TEST_URL); 
        });

        it('has a title', () => {     
            expect(suggestion.title).toEqual(TEST_TITLE);
        });

        it('has a description', () => {
            expect(suggestion.description).toEqual(TEST_DESCR);
        });

        it('has an url', () => {
            expect(suggestion.url).toEqual(TEST_URL);
        });
    });
   
});